# 明道云私有部署版

[![ming](https://svg.hamm.cn/badge.svg?key=I%20%E2%9D%A4%20MY%20TEAM&value=%E6%98%8E&color=ff4500)](https://www.mingdao.com) [![star](https://gitee.com/mingdaocloud/pd/badge/star.svg?theme=gvp)](https://gitee.com/mingdaocloud/pd/stargazers)
[![fork](https://gitee.com/mingdaocloud/pd/badge/fork.svg?theme=gvp)](https://gitee.com/mingdaocloud/pd/members)

<img src="https://user-images.githubusercontent.com/7261408/82203093-67ae1600-9935-11ea-8cd9-89b61b47b38f.png" alt="logo" height="150px"/>

[明道云](https://www.mingdao.com) 是一个企业软件的快速设计和开发工具。但不需要任何代码编写，普通业务人员就能掌握使用。通过灵活的功能组件，让企业可以搭建个性化的 CRM、ERP、OA、项目管理、进销存等系统，你可以用它管理生产、销售、采购、人事等所有企业活动。明道云私有部署基于镜像模式，旨在提供给用户一个能快速安装与体验的 APaaS 平台。

# 快速体验

- [快速安装](https://docs.pd.mingdao.com/deployment/docker-compose/standalone/quickstart.html)

# 文档

- [文档中心](https://docs.pd.mingdao.com)

# 联系我们

- 提交 issue
- 零代码社区 http://bbs.mingdao.net

# 许可证
[Apache 2.0 License.](/LICENSE)   